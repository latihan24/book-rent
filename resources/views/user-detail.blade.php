@extends('layout.mainlayout')

@section('title', 'Users Detail')
@section('content')
<h1>Detail User</h1>

<div class="mt-5 d-flex justify-content-end">
    @if ($user->status == 'inactive')
    <a href="/user-approve/{{ $user->slug }}" class="btn btn-warning">Approve User</a>
    @endif
    
</div>


<div clas="mt-5">
    @if(session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif
</div>

<div class="my-5 w-25">
    <div class="mb-3">
        <label for="" class="formlabel">Username</label>
        <input type="text" class="form-control" reandoly value="{{ $user->username }}">
    </div>
    <div class="mb-3">
        <label for="" class="formlabel">Phone</label>
        <input type="text" class="form-control" reandoly value="{{ $user->phone }}">
    </div>
    <div class="mb-3">
        <label for="" class="formlabel">Address</label>
        <textarea name="" id="" cols="30" rows="7" class="form-control" style="resize:none"></textarea>
    </div>
    <div class="mb-3">
        <label for="" class="formlabel">Status</label>
        <input type="text" class="form-control" reandoly value="{{ $user->status }}">
    </div>
</div>

<div class="mt-5">
    <h2>User's Rent Log</h2>
    <x-rent-log-table :rentlog='$rent_logs'/>
</div>
@endsection